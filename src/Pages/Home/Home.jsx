import React, { useState } from "react";
import bread1 from "../../../assets/bread.png";
import bread2 from "../../../assets/creambread.png";
import bread3 from "../../../assets/baguette.png";
import "../../App.css";
import "../../index.css";
import TypewriterComponent from "typewriter-effect";
import { Link } from "react-scroll";

function Home() {
  const [smallDiv1, setSmallDiv1] = useState("bg-orange-500");
  const [smallDiv2, setSmallDiv2] = useState("bg-white");
  const [smallDiv3, setSmallDiv3] = useState("bg-white");
  const [breadImage, setBreadImage] = useState(bread1);

  function change() {
    if (breadImage === bread1) {
      setTimeout(() => {
        setBreadImage(bread2);
        setSmallDiv1("bg-white");
        setSmallDiv2("primaryBackground");
        setSmallDiv3("bg-white");
      }, 3000);
    }

    if (breadImage === bread2) {
      setTimeout(() => {
        setBreadImage(bread3);
        setSmallDiv1("bg-white");
        setSmallDiv2("bg-white");
        setSmallDiv3("primaryBackground");
      }, 3000);
    }

    if (breadImage === bread3) {
      setTimeout(() => {
        setBreadImage(bread1);
        setSmallDiv1("primaryBackground");
        setSmallDiv2("bg-white");
        setSmallDiv3("bg-white");
      }, 3000);
    }
  }
  const breadTypes = ["BREADS", "DONUTS", "CAKES", "PIZZA", "OTHER PASTRIES"];
  return (
    <div className="flex flex-col pl-7 pr-7 justify-center homePage md:px-28 w-full">
      <div className="flex md:flex-row md:justify-between items-center justify-center flex-col w-full">
        <div className="flex flex-col md:w-6/12 xl:w-7/12 w-full">
          <h5 className="md:text-md text-white font-semibold text-base tracking-normal">
            <TypewriterComponent
              onInit={(typewriter) => {
                typewriter.typeString("Welcome at").start();
              }}
              options={{ cursor: "" }}
            />
          </h5>
          <h3 className="xl:text-5xl primaryColor font-extrabold text-4xl">
            <TypewriterComponent
              onInit={(typewriter) => {
                typewriter.pauseFor(1500).typeString("OMEGA BAKERY").start();
              }}
              options={{ delay: 100 }}
            />
          </h3>
          <p className="mt-10 text-white opacity-80 describePar text-justify">
            We are a bakery that makes and sell low-calorie pastries. Our main
            product is made with attention to detail and served in your favorite
            meals.{" "}
          </p>
          <div className="flex flex-row mt-10 xl:mt-20 ">
            <Link
              to="svgAboveProducts"
              smooth={true}
              className=" w-48 flex items-center justify-center py-4 font-bold text-xs tracking-widest rounded-full"
              style={{ backgroundColor: "var(--primaryColor)" }}
            >
              OUR PRODUCTS
            </Link>
            <a
              href="#"
              className=" w-48 flex items-center justify-center py-4 font-bold text-xs tracking-widest rounded-full text-white border ml-5"
            >
              REACH US
            </a>
          </div>
        </div>
        <div className="glassy mx-auto flex flex-col xl:w-96 xl:h-96 md:w-80 w-full h-72 rounded-lg px-5 md:m-0 md:ml-5 mt-7">
          <img
            className="xl:h-72 w-60 object-contain m-auto"
            src={breadImage}
            alt=""
          />
          <div className="flex flex-row md:mt-0  w-10 justify-between m-auto">
            <div className={` w-1.5 h-1.5 rounded-full ${smallDiv1}`} />
            <div className={` w-1.5 h-1.5   rounded-full ${smallDiv2}`} />
            <div className={` w-1.5 h-1.5  rounded-full ${smallDiv3}`} />
          </div>
        </div>
      </div>
      {change()}
      <ul className="glassy xl:w-3/5 md:text-xs  text-white flex flex-row justify-between py-5 xl:ml-64 mt-5 md:mt-16 font-semibold xl:px-10 px-5 breadTypes">
        {breadTypes.map((type) => (
          <li className="hover:text-orange-500 cursor-pointer">{type}</li>
        ))}
      </ul>
    </div>
  );
}

export default Home;
