import React from "react";
import ProductCard from "../../Components/ProductCard/ProductCard";
import Subtitle from "../../Components/Subtitle";
import classes from "./Products.module.scss";
import * as productsJson from "./productslist.json";
var products = productsJson.default;
export default function Products() {
  return (
    <section
      className={
        classes.products +
        " w-full flex flex-col flex-wrap justify-start items-center py-5 pt-20 md:flex-col"
      }
    >
      <Subtitle title="Our products" />
      <section className="flex flex-row flex-wrap justify-between m-auto mt-10 sm:mt-16 w-full md:w-11/12">
        {products &&
          products.map((product, i) => (
            <ProductCard
              key={i}
              name={product.name}
              ingredients={product.ingredients}
              img={product.img}
              quantity={product.quantity}
            />
          ))}
      </section>
    </section>
  );
}
