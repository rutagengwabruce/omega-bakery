import "./App.css";
import Nav from "./Components/Navbar/Nav";
import Products from "./Pages/Products/Products";
import Home from "./Pages/Home/Home";
import MobileNav from "./Components/Navbar/MobileNav";

function App() {
  return (
    <div className="App">
      <MobileNav />
      <Nav />
      <Home />
      <svg
        className="svgAboveProducts -mt-36"
        width="100%"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M0 128.295C130.424 170.208 1333.22 53.158 1442.16 0V184H0V151C0 132.561 9.79744e-06 135.018 1.27385e-05 136.48L0 128.295Z"
          fill="#FFF7F0"
        />
      </svg>
      <Products />
    </div>
  );
}

export default App;
