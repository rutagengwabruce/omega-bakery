import React, { useState, useEffect } from "react";
import { Instagram, WhatsApp } from "@material-ui/icons";

function Nav() {
  const [location1, setLocation1] = useState(
    "text-orange-500 border-orange-500"
  );
  const [location2, setLocation2] = useState(
    "text-white border-b-black  hover:text-orange-700 border-none"
  );
  const [location3, setLocation3] = useState(
    "text-white border-b-black  hover:text-orange-700 border-none"
  );
  const [location4, setLocation4] = useState(
    "text-white border-b-black  hover:text-orange-700 border-none"
  );
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }, [scroll]);
  return (
    <nav
      className={
        (scroll ? "glassy_nav" : "") +
        " md:flex hidden flex-row bg-none fixed w-full justify-between align-middle px-20"
      }
      style={{ zIndex: 1 }}
    >
      <img className="w-32 mt-3" src="assets/omega.png" alt="omegaBakeryLogo" />
      <ul
        className="flex flex-row mt-14 w-4/12 justify-between ml-28"
        style={{ fontSize: ".65em" }}
      >
        <li
          className={` h-7 font-semibold cursor-pointer   border-b-2 ${location1} `}
          onClick={() => {
            setLocation1("navComponent");
            setLocation2("text-white border-b-black hover:text-orange-700");
            setLocation3("text-white border-b-black hover:text-orange-700");
            setLocation4("text-white border-b-black hover:text-orange-700");
          }}
        >
          HOME
        </li>
        <li
          className={`h-10 font-semibold cursor-pointer   border-b-2 ${location2} `}
          onClick={() => {
            setLocation2("navComponent");
            setLocation1(
              "text-white border-b-black hover:text-orange-700 border-none"
            );
            setLocation3(
              "text-white border-b-black hover:text-orange-700 border-none"
            );
            setLocation4(
              "text-white border-b-black hover:text-orange-700 border-none"
            );
          }}
        >
          PRODUCTS
        </li>
        <li
          className={`h-10 font-semibold cursor-pointer   border-b-2 ${location3} `}
          onClick={() => {
            setLocation3("navComponent");
            setLocation1(
              "text-white border-b-black hover:text-orange-700 border-none"
            );
            setLocation2(
              "text-white border-b-black hover:text-orange-700 border-none"
            );
            setLocation4(
              "text-white border-b-black hover:text-orange-700 border-none"
            );
          }}
        >
          ABOUT US
        </li>
        <li
          className={`h-10 font-semibold cursor-pointer border-b-2 ${location4} `}
          onClick={() => {
            setLocation4("navComponent");
            setLocation1(
              "text-white border-b-black hover:text-orange-700 border-none"
            );
            setLocation2(
              "text-white border-b-black hover:text-orange-700 border-none"
            );
            setLocation3(
              "text-white border-b-black hover:text-orange-700 border-none"
            );
          }}
        >
          LOCATION
        </li>
      </ul>
      <div className="flex flex-row w-28 justify-evenly mt-14 h-10">
        <Instagram className="text-white cursor-pointer" fontSize="small" />
        <WhatsApp className="text-white cursor-pointer" fontSize="small" />
      </div>
    </nav>
  );
}

export default Nav;
