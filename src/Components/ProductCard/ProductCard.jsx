import React from "react";
import classes from "./ProductCard.module.scss";

export default function ProductCard({
  name,
  ingredients,
  price,
  img,
  quantity,
}) {
  return (
    <div
      className={
        "mx-3 my-4 md:mx-2 p-10 bg-white shadow-md rounded-xl flex flex-row w-100 items-center justify-between " +
        classes.productCard
      }
    >
      <div>
        <h1 className="text-6xl ">{name}</h1>
        <h6 className="mt-7 mb-2 font-semibold text-sm">Ingredients</h6>
        <p className="text-sm font-regular text-black opacity-75">
          {ingredients}
        </p>
        <h6 className="mt-7 mb-2 font-semibold text-sm">Price</h6>
        <p>
          <strong style={{ color: "var(--primaryColor)" }}>700 Rwf</strong>
          &nbsp;&nbsp;
          <span className=" text-xs inline-block ml-6">{quantity}</span>
        </p>
      </div>
      <img src={img} alt=" " className="w-5/12 -rotate-45 " />
    </div>
  );
}
