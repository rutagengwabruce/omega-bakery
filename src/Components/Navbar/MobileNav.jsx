import React, { useState, useEffect } from "react";
import { Instagram, WhatsApp } from "@material-ui/icons";
import { motion } from "framer-motion";

export default function MobileNav() {
  const variants = {
    open: { opacity: 1, x: 0 },
    closed: { opacity: 0, x: "-100%" },
  };
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }, [scroll]);
  return (
    <nav
      className={
        (scroll ? "glassy_nav" : "") +
        " flex flex-row w-full md:hidden justify-between items-center fixed bg-none px-6 py-3 text-white"
      }
      style={{ zIndex: 1 }}
    >
      <div className="w-1/12">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="20"
          viewBox="0 0 1000 1000"
          fill="white"
        >
          <g>
            <path d="M990,210.3c0,38.7-31.3,70-70,70H80c-38.7,0-70-31.3-70-70l0,0c0-38.7,31.3-70,70-70H920C958.5,140.2,990,171.6,990,210.3L990,210.3L990,210.3z" />
            <path d="M713.4,500c0,38.7-31.3,70-70,70H80c-38.7,0-70-31.3-70-70l0,0c0-38.7,31.3-70,70-70l563.3,0C682,430,713.4,461.3,713.4,500L713.4,500L713.4,500z" />
            <path d="M503.4,789.7c0,38.7-31.3,70-70,70H80c-38.7,0-70-31.3-70-70l0,0c0-38.7,31.3-70,70-70h353.4C472.1,719.7,503.4,751.2,503.4,789.7L503.4,789.7L503.4,789.7z" />
          </g>
        </svg>
      </div>
      <img src="assets/omega.png" alt=" " className="w-5/12" />
      <div className="flex flex-row justify-end items-center text-white w-3/12">
        <Instagram className=" cursor-pointer" fontSize="medium" />
        <WhatsApp className=" ml-7 cursor-pointer" fontSize="medium" />
      </div>
    </nav>
  );
}
