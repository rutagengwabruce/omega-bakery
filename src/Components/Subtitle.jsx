import React from "react";

export default function Subtitle({ title }) {
  return (
    <div className="w-full text-center">
      <h5 className="sub-title text-xs uppercase">{title}</h5>
      <hr className="w-3 m-auto h-0.5 bg-black" />
    </div>
  );
}
